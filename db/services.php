<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Reading list plugin to maintain a list of books or URL's to refer to
 *
 * @package
 * @copyright  2021 Jon Deavers jonathan.deavers@moodle.com
 * @license    https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

$functions = array(
    'local_reading_list_delete_item' => array(         /* Web service function name */
        'classname'   => 'local_reading_list_external',  /* Class containing the external function OR namespaced class */
        /* in classes/external/XXXX.php */
        'methodname'  => 'delete_item',          /* External function name */
        'classpath'   => 'local/reading_list/externallib.php',  /* File containing the class/external function - */
        /* not required if using namespaced auto-loading classes. */
        'description' => 'Delete a list item.',    /* Human readable description of the web service function */
        'type'        => 'write',                  /* Database rights of the web service function (read, write) */
        'ajax' => true,        /* Is the service available to 'internal' ajax calls? */
        'capabilities' => '', /* Comma separated list of capabilities used by the function. */
    ),
);

