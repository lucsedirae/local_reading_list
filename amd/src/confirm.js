// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Reading list plugin to maintain a list of books or URL's to refer to
 *
 * @package
 * @copyright  2021 Jon Deavers jonathan.deavers@moodle.com
 * @license    https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define(['jquery', 'core/modal_factory', 'core/str', 'core/modal_events', 'core/ajax', 'core/notification'], function($, ModalFactory, String, ModalEvents, Ajax, Notification) {
    var trigger = $('.local_reading_list_delete_button');
    ModalFactory.create({
        type: ModalFactory.types.SAVE_CANCEL,
        title: String.get_string('delete_item', 'local_reading_list'),
        body: String.get_string('delete_item_confirm', 'local_reading_list'),
        preShowCallback: function(triggerElement, modal) {
            triggerElement = $(triggerElement);
            let classString = triggerElement[0].classList[0];
            let itemid = classString.substr(classString.lastIndexOf('local_reading_listid') + 'local_reading_listid'.length);
            modal.params = {'itemid': itemid};
            modal.setSaveButtonText(String.get_string('delete_item', 'local_reading_list'));
        },
        large: true,
    }, trigger)
        .done(function(modal) {
            // Do what you want with your new modal.
            modal.getRoot().on(ModalEvents.save, function(e) {
                e.preventDefault();
                let request = {
                    methodname: 'local_reading_list_delete_item',
                    args: modal.params,
                };

                Ajax.call([request])[0].done(function(data) {
                    if (data === true) {
                        window.location.reload();
                    } else {
                        Notification.addNotification({
                            message: String.get_string('delete_item_fail', 'local_reading_list'),
                            type: 'error'
                        });
                    }
                }).fail(Notification.exception);

            });
        });
});