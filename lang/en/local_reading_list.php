<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Reading list plugin to maintain a list of books or URL's to refer to
 *
 * @package    local_reading_list
 * @copyright  2021 Jon Deavers jonathan.deavers@moodle.com
 * @license    https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['add_button'] = 'Submit';
$string['author_desc'] = 'Author of title';
$string['author_label'] = 'Author';
$string['author_placeholder'] = "Author";
$string['create_success'] = 'List item created';
$string['delete_item'] = "Delete";
$string['delete_item_confirm'] = "Confirm item deletion?";
$string['delete_item_fail'] = "Item deletion failed";
$string['edit_cancelled'] = "Form cancelled";
$string['editform'] = "Edit form";
$string['editform_desc'] = "Select fields to appear in list item form";
$string['form_header'] = "Edit list items";
$string['notes_label'] = 'User notes';
$string['notes_desc'] = 'User notes about title';
$string['permission_denied'] = 'You do not have permission to perform this action.';
$string['pluginname'] = 'Reading list';
$string['public_label'] = 'Public list item';
$string['reading_list:allowcreatepublic'] = "Public list item";
$string['reading_list:allowdeletepublic'] = "Delete public list item";
$string['reading_list_title'] = 'My reading list';
$string['title_label'] = 'Book/article title';
$string['title_placeholder'] = 'Enter title';
$string['update_success'] = 'List item updated';
$string['url_label'] = 'URL';
