<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Reading list plugin to maintain a list of books or URL's to refer to
 *
 * @package    local_reading_list
 * @copyright  2021 Jon Deavers jonathan.deavers@moodle.com
 * @license    https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace local_reading_list\form;
defined('MOODLE_INTERNAL') || die();
use moodleform;
require_once("$CFG->libdir/formslib.php");

class edit extends moodleform {
    public function definition() {
        global $CFG;
        $formsettings = new \stdClass();
        $formsettings->author = get_config('local_reading_list', 'author');
        $formsettings->notes = get_config('local_reading_list', 'notes');

        $mform = $this->_form;

        $mform->addElement('header', 'formheader', get_string('form_header', 'local_reading_list'));

        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);

        $mform->addElement('hidden', 'userid');
        $mform->setType('userid', PARAM_INT);

        $mform->addElement('text', 'title', get_string('title_label', 'local_reading_list'));
        $mform->setType('title', PARAM_NOTAGS);
        $mform->setDefault('title', get_string('title_placeholder', 'local_reading_list'));

        $mform->addElement('text', 'url', get_string('url_label', 'local_reading_list'));
        $mform->setType('url', PARAM_URL);

        /* This if/else solution is inelegant. Recommend rethinking settings handling. Switch may be better option */
        /* Switch evaluation could also keep database from storing placeholder value */
        if ($formsettings->author == '1') {
            $mform->addElement('text', 'author', get_string('author_label', 'local_reading_list'));
            $mform->setType('author', PARAM_NOTAGS);
            $mform->setDefault('author', get_string('author_placeholder', 'local_reading_list'));
        } else {
            $mform->addElement('hidden', 'author', get_string('author_label', 'local_reading_list'));
            $mform->setType('author', PARAM_NOTAGS);
            $mform->setDefault('author', get_string('author_placeholder', 'local_reading_list'));
        }

        if ($formsettings->notes == '1') {
            $mform->addElement('textarea', 'notes', get_string('notes_label', 'local_reading_list'));
            $mform->setType('notes', PARAM_NOTAGS);
        } else {
            $mform->addElement('hidden', 'notes', get_string('notes_label', 'local_reading_list'));
            $mform->setType('notes', PARAM_NOTAGS);
        }

        /* Check capabilities to create public list item */
        $context = \context_system::instance();
        if (has_capability('local/reading_list:allowcreatepublic', $context)) {
            $mform->addElement('checkbox', 'public', get_string('public_label', 'local_reading_list'));
        } else {
            $mform->addElement('hidden', 'public');
        }
        $mform->setType('public', PARAM_BOOL);
        $mform->setDefault('public', false);

        $this->add_action_buttons(true, get_string('add_button', 'local_reading_list'));
    }

    public function validation($data, $files) {
        return array();
    }

    public function has_config() {
        return true;
    }
}
