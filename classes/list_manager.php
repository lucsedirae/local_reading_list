<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Reading list plugin to maintain a list of books or URL's to refer to
 *
 * @package    local_reading_list
 * @copyright  2021 Jon Deavers jonathan.deavers@moodle.com
 * @license    https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace local_reading_list;
use core\notification;
use dml_exception;
use stdClass;

class list_manager
{
    /**
     * @param string $itemtitle
     * @param string $itemurl
     * @param string $itemauthor
     * @param string $itemnotes
     * @return bool true if successful
     */
    public function create_list_item(string $itemtitle, string $itemurl, $itemauthor, $itemnotes, $public): bool {
        global $USER, $DB;
        $formsettings = new \stdClass();
        $formsettings->author = get_config('local_reading_list', 'author');
        $formsettings->notes = get_config('local_reading_list', 'notes');

        $recordtoinsert = new stdClass();
        $recordtoinsert->userid = $USER->id;
        $recordtoinsert->title = $itemtitle;
        $recordtoinsert->url = $itemurl;
        $recordtoinsert->author = $itemauthor;
        $recordtoinsert->notes = $itemnotes;
        $recordtoinsert->public = $public;

        try {
            return $DB->insert_record('local_reading_list', $recordtoinsert);
        } catch (dml_exception $e) {
            return false;
        }
    }

    /**
     * Get single item from list
     * @param $itemid id of item requested
     * @return object|false return item or false if not found
     */
    public function get_item(int $itemid) {
        global $DB;
        return $DB->get_record('local_reading_list', ['id' => $itemid]);
    }

    /**
     * Update single item in list
     * @param $id Id of item being updated
     * @param $title Title of book/article
     * @param $url url of book/article
     * @param $author author of book/article
     * @param $notes User notes
     * @return bool item data or false if not found
     */
    public function update_item(int $id, string $title, string $url, string $author, string $notes, int $public) {
        global $USER, $DB;
        $object = new stdClass();
        $object->id = $id;
        $object->userid = $USER->id;
        $object->title = $title;
        $object->url = $url;
        $object->author = $author;
        $object->notes = $notes;
        $object->public = $public;
        $DB->update_record('local_reading_list', $object);
    }

    /**
     * Delete item from list
     * @param $itemid id of item requested
     * @return bool return true if successful
     * @throws \dml_transaction_exception
     */
    public function delete_item(int $itemid) {
        global $DB;
        $transaction = $DB->start_delegated_transaction();
        $deleteditem = $DB->delete_records('local_reading_list', ['id' => $itemid]);
        if ($deleteditem) {
            $DB->commit_delegated_transaction($transaction);
        }
        return true;
    }

    /**
     * Delete public item from list
     * @param $itemid id of item requested
     * @return bool return true if successful
     * @throws dml_exception
     */
    public function delete_public_item(int $itemid) {
        global $DB;
        $context = \context_system::instance();
        $transaction = null;
        $deleteditem = null;
        $access = has_capability('local/reading_list:allowcreatepublic', $context);

        if ($access == true) {
            $transaction = $DB->start_delegated_transaction();
            $deleteditem = $DB->delete_records('local_reading_list', ['id' => $itemid]);
        }

        if ($deleteditem) {
            $DB->commit_delegated_transaction($transaction);
        } else {
            notification::add(get_string('permission_denied', 'local_reading_list'), \core\output\notification::NOTIFY_ERROR);
        }
        return true;
    }
}
