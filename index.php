<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Reading list plugin to maintain a list of books or URL's to refer to
 *
 * @package    local_reading_list
 * @copyright  2021 Jon Deavers jonathan.deavers@moodle.com
 * @license    https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use local_reading_list\form\edit;
use local_reading_list\list_manager;

require_once(__DIR__ . '/../../config.php');

global $USER, $DB;

/* Page setup */
$PAGE->set_url(new moodle_url('/local/reading_list/index.php'));
$PAGE->set_context(\context_system::instance());
$PAGE->set_title(get_string('reading_list_title', 'local_reading_list'));
$PAGE->set_heading(get_string('reading_list_title', 'local_reading_list'));
$PAGE->requires->js_call_amd('local_reading_list/confirm');

require_login();

/* Retrieve list */
$useritems = $DB->get_records('local_reading_list', ['userid' => $USER->id ], 'id');
$publicitems = $DB->get_records('local_reading_list', ['public' => 1], 'id');
$items = array_unique(array_merge($useritems, $publicitems), SORT_REGULAR);
$itemid = optional_param('itemid', null, PARAM_INT);
$public = optional_param('public', null, PARAM_INT);

/* Form handling */
$mform = new edit();

if ($mform->is_cancelled()) {
    redirect($PAGE->url, get_string('edit_cancelled', 'local_reading_list'), null, \core\output\notification::NOTIFY_SUCCESS);
} else if ($fromform = $mform->get_data()) {
    $listmanager = new list_manager();
    if ($fromform->id) {
        /* If form has an id value, indicates item is being updated */
        $listmanager->update_item($fromform->id, $fromform->title, $fromform->url, $fromform->author,
            $fromform->notes, $fromform->public);
        redirect($PAGE->url, get_string('update_success', 'local_reading_list'), null, \core\output\notification::NOTIFY_SUCCESS);
    }
    $listmanager->create_list_item($fromform->title, $fromform->url, $fromform->author, $fromform->notes, $fromform->public);
    redirect($PAGE->url, get_string('create_success', 'local_reading_list'), null, \core\output\notification::NOTIFY_SUCCESS);
}

/* Parse parameters for item id # to handle item editing in form*/
if ($itemid) {
    $listmanager = new list_manager();
    $item = $listmanager->get_item($itemid);
    if (!$item) {
        throw new invalid_parameter_exception('Item not found');
    }
    $mform->set_data($item);
}

/* Output */
$templatecontext = (object)[
    'items' => array_values($items),
    'editurl' => $PAGE->url,
];

echo $OUTPUT->header();
$mform->display();
echo $OUTPUT->render_from_template('local_reading_list/reading_list', $templatecontext);
echo $OUTPUT->footer();
