<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Local plugin that allows messages to be shared to user and keeps records of messages read by users.
 *
 * @package    local_message
 * @category   messages
 * @copyright  none - following tutorial from https://www.udemy.com/course/moodle-developer-course/
 * @license    https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

if ($hassiteconfig) {
    $ADMIN->add('localplugins', new admin_category('local_reading_list_settings', get_string('pluginname', 'local_reading_list')));
    $settingspage = new admin_settingpage('managelocalreadinglist', get_string('pluginname', 'local_reading_list'));

    if ($ADMIN->fulltree) {
        $settingspage->add(new admin_setting_heading('editform', get_string('editform', 'local_reading_list'),
            get_string('editform_desc', 'local_reading_list')));
        $settingspage->add(new admin_setting_configcheckbox('local_reading_list/author',
            get_string('author_label', 'local_reading_list'),
            get_string('author_desc', 'local_reading_list'),
        1
        ));    $settingspage->add(new admin_setting_configcheckbox('local_reading_list/notes',
            get_string('notes_label', 'local_reading_list'),
            get_string('notes_desc', 'local_reading_list'),
        1
        ));

    }

    $ADMIN->add('localplugins', $settingspage);
}
