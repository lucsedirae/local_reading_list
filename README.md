# Moodle Reading List Plugin
Reading list is a local plugin that allows users to save book or article titles, author info, URL, and notes on the 
title.

## Table of contents

[Version](#version)

[Installation](#installation)

[Features](#features)

[Usage](#usage)

## Version
V1.01 (2021120800)

Requires Moodle version 3.11 (2021051700)

## Installation
Git clone repository to the `local` directory of your Moodle installation

`git clone git@bitbucket.org:lucsedirae/local_reading_list.git`

Run `php admin/cli/upgrade.php` from command line or reload browser to initialize install

## Features

- /reading_list/db/install.xml creates table "local_reading_list" and 5 data fields
- "Reading list" added to front page navigation block

## Usage

##Todo

- Troubleshoot non admin user login
- Remove delete functionality from public items for non admin/teachers



