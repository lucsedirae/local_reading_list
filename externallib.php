<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Reading list plugin to maintain a list of books or URL's to refer to
 *
 * @package
 * @copyright  2021 Jon Deavers jonathan.deavers@moodle.com
 * @license    https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') ||  die();

use local_reading_list\list_manager;
require_once($CFG->libdir . "/externallib.php");

class local_reading_list_external extends external_api {
    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function delete_item_parameters() {
        return new external_function_parameters(
            ['itemid' => new external_value(PARAM_INT, 'id of list item')],
        );
    }

    /**
     * Function to be executed
     * @return string welcome message
     */
    public static function delete_item($itemid): string {
        global $DB;

        $item = $DB->get_record('local_reading_list', ['id' => $itemid]);

        $params = self::validate_parameters(self::delete_item_parameters(), array('itemid' => $itemid));

        $manager = new list_manager();

        if ($item->public === '1') {
                return $manager->delete_public_item($itemid);
        } else {
            return $manager->delete_item($itemid);
        }
    }

    /**
     * Returns description of method result value
     * @return external_description
     */
    public static function delete_item_returns() {
        return new external_value(PARAM_BOOL, 'True if item was successfully deleted.');
    }
}
